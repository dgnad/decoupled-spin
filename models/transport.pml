#define N_TRUCKS 1
#define N_PACKAGES 4

#define SIZE 2

byte TRUCK_POS[N_TRUCKS]

active [N_TRUCKS] proctype truck()
{
end:do
	:: (TRUCK_POS[(_pid % N_TRUCKS)] < SIZE) ->
          TRUCK_POS[(_pid % N_TRUCKS)] = TRUCK_POS[(_pid % N_TRUCKS)] + 1;
    :: (TRUCK_POS[(_pid % N_TRUCKS)] > 0) -> 
          TRUCK_POS[(_pid % N_TRUCKS)] = TRUCK_POS[(_pid % N_TRUCKS)] - 1;
	od
}

active [N_PACKAGES] proctype package()
{
	byte pos = 0;
	byte goal = SIZE;
	byte t = 0;
	do
	:: (pos > SIZE) -> /* package is loaded */
	      do
	         :: pos = TRUCK_POS[pos - SIZE - 1]; /* unload */
	            break;
	         :: break; /* remain in truck */
          od
          if 
          :: (pos == goal) ->
	         break;
          fi
    :: (pos <= SIZE) ->
       if
	   :: (TRUCK_POS[t] == pos) ->
	      pos = SIZE + t + 1;
       :: else ->
          t = (t + 1) % N_TRUCKS;
       fi
	od
}
