#define N 6

mtype {request,result}

chan responses[N] = [4] of { mtype }
chan requests = [1] of { mtype , byte }

active [N] proctype client()
{
	bool waiting = false;
	do
	:: requests!request,(_pid%N);
	   waiting = true;
	   responses[_pid%N]?result
	   waiting = false;
	od
}

active proctype server()
{
	byte who;
	do
	:: requests?request,who;
	   responses[who]!result
	od
}

