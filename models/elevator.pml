#define true 1
#define false 0

#define LEVELS 5

/*#include "Elevator.prm"*/
/* Global Variables */
bool elevator_opened=false;
int elevator_pos=0;
bool elevator_stopped=true;
bool elevator_upward=false;

/* Events */
bool elevator_tick;

active proctype elevator()
{

	/* Set up initial values */
	elevator_tick=0;
	elevator_pos=0;
	elevator_stopped=true;
	elevator_upward=true;
	elevator_opened=false;

	/* Wait for ticks */
	do
	::elevator_tick->
		if
		::!elevator_stopped ->
			if
			::elevator_upward ->
		    		if
				::(elevator_pos < (LEVELS-1) ) ->
					printf("Elevator moving upward to pos %d\n",elevator_pos+1);
					elevator_pos++
				::else -> elevator_stopped = true
		  		fi
			::else ->
				if
				::(elevator_pos > 0) ->
					printf("Elevator moving downward to pos %d\n",elevator_pos-1);
					elevator_pos--
		    		::else -> elevator_stopped = true
				fi
			fi
		::else -> skip
		fi;
		elevator_tick=0
	od
}

/*#include "Level.prm"*/
/* Global Variables */
bool level_requested[LEVELS];

active [LEVELS] proctype level()
{
	int pos = _pid%LEVELS

	level_requested[pos]=0;
	do
	::!level_requested[pos] ->
		printf("Request on level %d\n",pos);
		level_requested[pos]=true
		/* Whenever there is no request in this level,
		a request can arise */
	od
}

/*#include "SafeController.prm"*/
/* Events */
bool controller_tick;

active proctype controller()
{
	int target;

	controller_tick=0;

	do
	::controller_tick ->
		if
		::(target < LEVELS) ->
			if
			::(target == elevator_pos) ->
				if
				::(elevator_opened) ->
					printf("Controller closing the elevator in level %d...\n",elevator_pos);
					level_requested[target]=false;
					elevator_opened=false;
					target = LEVELS
				::((!elevator_opened) && elevator_stopped) ->
					printf("Controller opening the elevator in level %d...\n",elevator_pos);
					elevator_opened=true
			    	::else ->
					printf("Controller stopping the elevator in level %d...\n",elevator_pos);
					elevator_stopped=true
				fi
			::else ->
				printf("Controller moving the elevator...\n");
				elevator_stopped=false;
				elevator_upward=(target > elevator_pos)
			fi
		::else ->
			/*target=0;
			do
			::(target < LEVELS) ->
		      		if
				::level_requested[target] -> break
				::else -> skip
				fi;
				target++
			::(target >= LEVELS) -> break
			od*/
			if
			::level_requested[LEVELS-1] -> target=LEVELS-1
			::level_requested[2] -> target=2
			::level_requested[1] -> target=1
			::level_requested[0] -> target=0
			fi
		fi;
		controller_tick=0
	od
}


active proctype main()
{
	do
	::skip ->
		d_step{ controller_tick=1; elevator_tick=1; }
		elevator_tick==0 && controller_tick==0;
	od
}
