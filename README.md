building and running works like usual in SPIN:

cd src

make                          // builds spin

./spin -a -o3 MODEL           // compile promela model (statement merging is not supported, yet (-o3))

cc -o pan pan.c -DNOREDUCE    // compile verifier (partial-order reduction is not supported, yet (-DNOREDUCE))

./pan                         // run verifier